package main.java.com.GuessNumber.memory;

public class GenerateRandom {

    private int start;
    private int end;
    private int timesToGuess;
    private int givenAnswer;
    private int previousGuess;

    //конструктор

    public GenerateRandom(int start, int end, int timesToGuess) {
        this.start = start;
        this.end = end;
        this.timesToGuess = timesToGuess;
    }

    public GenerateRandom(int start,int end){
        this.start = start;
        this.end = end;
    }

    //геттеры сеттеры


    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public int getTimesToGuess() {
        return timesToGuess;
    }

    public void setTimesToGuess(int timesToGuess) {
        this.timesToGuess = timesToGuess;
    }

    public int getGivenAnswer() {
        return givenAnswer;
    }

    public void setGivenAnswer(int givenAnswer) {
        this.givenAnswer = givenAnswer;
    }

    public int getPreviousGuess() {
        return previousGuess;
    }

    public void setPreviousGuess(int previousGuess) {
        this.previousGuess = previousGuess;
    }

    //метод выдавающий рандом
    public int getNextRandom(){

        int random = Math.abs(start) + (int)(Math.random() * (Math.abs(end - start) + 1));


        return random;
    }
}
