package main.java.com.GuessNumber.service;

public class GameRunnerService {
    private AnsweringBureauService abs = new AnsweringBureauService();
    private static final String MSG_WELCOME = ("Enter: help - to see list of command");

    public GameRunnerService() {
    }


    public void runner(){
        System.out.println(MSG_WELCOME);
        while(true){
            abs.menuLoop();
        }
    }
}
